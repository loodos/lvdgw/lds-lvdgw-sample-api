﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace LDS.LVDGW.Sample.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet("model")]
        public ActionResult<object> GetModel()
        {
            return new {
                processStatus = new {
                   code = 1001,
                   description = "model is empty",
                   success = false
                },
                api = new {
                    version = "1.0",
                    language = "tr",
                    path = "api/values/model"
                },
                friendlyMessage = new {
                    displayType = "POPUP",
                    title = "Error!",
                    description = "data cannot be accessed",
                    cancelable = true,
                    buttonSet = new[] {
                        new {
                            type = "POSITIVE",
                            text = "Okey",
                            action = "URL",
                            actionParameter = "http://lvdgw.com"
                        },
                        new {
                            type = "NEGATIVE",
                            text = "Close",
                            action = "CLOSE",
                            actionParameter = ""
                        }
                    }
                },
                data = new {
                    modelList = new[] {
                        new {
                            id = 1,
                            text = "lorem"
                        },
                        new {
                            id = 2,
                            text = "ipsum"
                        }
                    },
                    pageIndex = 0
                }
            };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return $"value {id}";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
